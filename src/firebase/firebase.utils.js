import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: 'AIzaSyDH-lewJGCJeu31yamJXFD81kHzycUFmdw',
    authDomain: 'react-commerce-e1c6f.firebaseapp.com',
    databaseURL: 'https://react-commerce-e1c6f.firebaseio.com',
    projectId: 'react-commerce-e1c6f',
    storageBucket: 'react-commerce-e1c6f.appspot.com',
    messagingSenderId: '388110590647',
    appId: '1:388110590647:web:ce7a729234c93cb0c8e332',
    measurementId: 'G-6Q1Z7PTCP0'
}

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return

    const userRef = firestore.doc(`users/${userAuth.uid}`)

    const snapShot = await userRef.get()

    if (!snapShot.exists) {
        const {displayName, email} = userAuth
        const createdAt = new Date()

        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        } catch (e) {
            console.log('error creating user', e.message)
        }
    }

    return userRef
}

firebase.initializeApp(config)

export const auth = firebase.auth()
export const firestore = firebase.firestore()

const provider = new firebase.auth.GoogleAuthProvider()
provider.setCustomParameters({prompt: 'select_account'})
export const signInWithGoogle = () => auth.signInWithPopup(provider)

export default firebase